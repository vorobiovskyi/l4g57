//
//  ViewController.swift
//  L4
//
//  Created by Богдан Воробйовський on 05.10.2017.
//  Copyright © 2017 Bohdan Vorobiovskyi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var inputTexField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let number = Double(inputTexField.text!)!
       
       Work.inches(inches: number)
        
    }
}


