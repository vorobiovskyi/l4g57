//
//  Work.swift
//  L4
//
//  Created by Богдан Воробйовський on 05.10.2017.
//  Copyright © 2017 Bohdan Vorobiovskyi. All rights reserved.
//

import UIKit

class Work: NSObject {
    static func printHello() {
        print("Hello world")
    }
    static func greeting(count: Int) {
        for _ in 0..<count {
            printHello()
        }
    }
    static func weekDay (variantsCount: Int = 7) {
        let numberToChoose = variantsCount
     
      
            var daysOfWeek = numberToChoose
            switch daysOfWeek {
            case 1:
                print("monday")
            case 2:
                print("Tuesday")
            case 3:
                print("wednesday")
            case 4:
                print("thursday")
            case 5:
                print("friday")
            case 6:
                print("saturday")
            case 7:
                print("sunday")
            default:
                print("Try again")
            }
    }
    static func inches(inches: Double) {
        print("inches", inches)
        let santInInch = 2.54
        var santimeters = inches * santInInch
        print("in", inches, " inches", santimeters, " santimeters")
    }
}
